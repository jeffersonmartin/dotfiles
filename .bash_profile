alias art='php artisan'
alias unit='vendor/bin/phpunit'
export PATH="$PATH:$HOME/.composer/vendor/bin"
export EDITOR=nano
export PATH="/usr/local/opt/ruby@2.5/bin:$PATH"
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
eval "$(rbenv init -)"