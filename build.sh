#!/bin/bash

# Build Script for Macbook Configuration
# gitlab.com/jeffersonmartin/dotfiles
# 
# Usage
# ~/Downloads/dotfiles-master/build.sh

# Define Variables for Syntax Highlighting Colors
C_DEFAULT='\033[m'
C_WHITE='\033[1m'
C_BLACK='\033[30m'
C_RED='\033[31m'
C_GREEN='\033[32m'
C_YELLOW='\033[33m'
C_BLUE='\033[34m'
C_PURPLE='\033[35m'
C_CYAN='\033[36m'
C_LIGHTGRAY='\033[37m'
C_DARKGRAY='\033[1;30m'
C_LIGHTRED='\033[1;31m'
C_LIGHTGREEN='\033[1;32m'
C_LIGHTYELLOW='\033[1;33m'
C_LIGHTBLUE='\033[1;34m'
C_LIGHTPURPLE='\033[1;35m'
C_LIGHTCYAN='\033[1;36m'

#
# Output - Script Header
#

printf "${C_PURPLE}\n"
printf "   /**\n"
printf "    *\n"
printf "    ${C_PURPLE}*   ${C_PURPLE}Build Script for Macbook Configuration\n"
printf "    ${C_PURPLE}*   \n"
printf "    ${C_PURPLE}*   ${C_DEFAULT}@package${C_PURPLE} gitlab.com/jeffersonmartin/dotfiles\n"
printf "    ${C_PURPLE}*   ${C_DEFAULT}@author${C_PURPLE}  Jeff Martin\n"
printf "    ${C_PURPLE}*   \n"
printf "    */\n"
printf "    \n${C_DEFAULT}"

printf ""
printf -e "${C_CYAN}Copy Bash Profile to Home Directory"
printf ""
cp .bash_profile ~/.bash_profile
source ~/.bash_profile

printf ""
printf -e "${C_CYAN}Installing Brew Package Manager"
printf ""
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

printf ""
printf -e "${C_CYAN}Updating the Brew Repositories and Upgrading Packages"
printf ""
brew update 
brew upgrade

printf ""
printf -e "${C_CYAN}Installing Nginx"
printf ""
brew install nginx
brew services start nginx

printf ""
printf -e "${C_CYAN}Installing PHP"
printf ""
brew install php@7.2 
brew services start php@7.2

printf ""
printf -e "${C_CYAN}Installing MySQL"
printf ""
brew install mysql@5.7
brew services start mysql@5.7

printf ""
printf -e "${C_CYAN}Installing Composer for PHP Package Management"
printf ""
brew install composer
# The composer PATH variable has already been set in ~/.bash_profile

printf ""
printf -e "${C_CYAN}Installing Laravel Installer"
printf ""
composer global require laravel/installer

printf ""
printf -e "${C_CYAN}Installing Laravel Valet for Hosting"
printf ""
composer global require laravel/valet
valet install
valet tld valet

printf ""
printf -e "${C_CYAN}Create Sites Directory for Git Repositories"
printf ""
mkdir ~/Sites
mkdir -e ~/Sites/github
mkdir -e ~/Sites/jeffersonmartin
mkdir -e ~/Sites/gitlab-com
mkdir -e ~/Sites/gitlab-org

printf ""
printf -e "${C_CYAN}Symlink Sites directory for Valet "
printf ""
cd ~/Sites/github && valet park
cd ~/Sites/jeffersonmartin && valet park
cd ~/Sites/gitlab-com && valet park 
cd ~/Sites/gitlab-org && valet park

printf ""
printf -e "${C_CYAN}Installing Git"
printf ""
brew install git

printf ""
printf -e "${C_CYAN}Installing Node Package Manager"
printf ""
brew install npm

printf ""
printf -e "${C_CYAN}Installing Yarn"
printf ""
brew install yarn

printf ""
printf -e "${C_CYAN}Installing Ansible"
printf ""
brew install ansible

printf ""
printf -e "${C_CYAN}Installing Ruby Environment Manager"
printf ""
brew install rbenv

printf ""
printf -e "${C_CYAN}Installing Ruby v2.5"
printf ""
brew install ruby@2.5

printf ""
printf -e "${C_CYAN}Installing Python"
printf ""
brew install python

printf ""
printf -e "${C_CYAN}Installing Docker"
printf ""
brew install docker

printf ""
printf -e "${C_CYAN}Installing Kubernetes Control"
printf ""
brew install kubctl

printf ""
printf -e "${C_CYAN}Installing DigitalOcean Control"
printf ""
brew install doctl

printf ""
printf -e "${C_CYAN}Installing Flux"
printf ""
brew cask install flux

printf ""
printf -e "${C_CYAN}Installing Google Chrome"
printf ""
brew cask install google-chrome

printf ""
printf -e "${C_CYAN}Installing Slack"
printf ""
brew cask install slack

printf ""
printf -e "${C_CYAN}Installing Zoom"
printf ""
brew cask install zoomus

printf ""
printf -e "${C_CYAN}Installing Telegram Desktop"
printf ""
brew cask install telegram-desktop

printf ""
printf -e "${C_CYAN}Installing Atom"
printf ""
brew cask install atom

printf ""
printf -e "${C_CYAN}Installing Sublime Text"
printf ""
brew cask install sublime-text

printf ""
printf -e "${C_CYAN}Installing iTerm2"
printf ""
brew cask install iterm2

printf ""
printf -e "${C_CYAN}Installing Sequel Pro"
printf ""
brew cask install sequel-pro

printf ""
printf -e "${C_CYAN}Installing Panic Transmit"
printf ""
brew cask install transmit

printf ""
printf -e "${C_CYAN}Installing GitHub Desktop"
printf ""
brew cask install github

printf ""
printf -e "${C_CYAN}Installing Spotify"
printf ""
brew cask install spotify

printf ""
printf -e "${C_CYAN}Installing Dropbox"
printf ""
brew cask install dropbox

printf ""
printf -e "${C_CYAN}Installing 1Password"
printf ""
brew cask install 1password

printf ""
printf -e "${C_CYAN}Installing Spectacle"
printf ""
brew cask install spectacle

printf ""
printf -e "${C_CYAN}Installing Nuclino"
printf ""
brew cask install nuclino

printf ""
printf -e "${C_CYAN}Installing Screenflow"
printf ""
brew cask install screenflow

printf ""
printf -e "${C_CYAN}Installing Fork"
printf ""
brew cask install fork

printf ""
printf -e "${C_CYAN}Installing Viscosity"
printf ""
brew cask install viscosity

printf ""
printf -e "${C_CYAN}Installing Vmware Fusion v8"
printf ""
brew cask install vmware-fusion8

printf ""
printf -e "${C_CYAN}Adding Application Icons to Dock"
printf ""

defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Google Chrome.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Slack.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/zoom.us.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Calendar.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Calculator.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Telegram Desktop.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Messages.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Sublime Text.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Atom.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/iTerm.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Sequel Pro.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Fork.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/GitHub Desktop.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Spotify.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/System Preferences.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
killall Dock

printf "\n        ${C_LIGHTGREEN}Configuration complete. May the force be with you!${C_DEFAULT}\n"
printf "\n"
