# Macbook Build Script

This is a script that I use with a fresh install of OS X to get all of my preferred applications installed with ease.

Feel free to use at your own risk or customize to your liking. 

## Instructions

1. Clone or download the repository to `~/Downloads` (and extract if needed).
2. Open Terminal
3. `~/Downloads/dotfiles-master/build.sh`
4. Perform additional configuration steps below.

## Additional Configuration Steps for Applications

1. Open Dropbox and sign in to start the syncing process. Configure selective sync if needed.
2. Open Google Chrome and install the following extensions:
    * 1Password
    * Clockwork
    * Google Translate
    * Zoom Scheduler
    * Vue.js devtools
    * JSON Viewer Awesome
3. Sign in with Google account(s). Bookmarks will auto-sync.
4. Open Flux and set preferences to 4900K (Daytime), 3500 (Sunset), 2500 (Bedtime).
5. Open 1Password and sign in. Locate vaults in Dropbox.
6. Download and install Sonos 
 

## Additional Configuration Steps for Mac OS

* System Preferences > General
    * Apperance: Dark
    * Accent Color: Purple
    * Default web browser: Chrome
* System Preferences > Desktop & Screensaver
    * Configure whatever you feel like
* System Preferences > Dock
    * Size: 15-20%
    * Magnification: 30-40%
    * Show recent applications in Dock: Disabled
* System Preferences > Security & Privacy
    * Require Password: 5 Minutes
    * Allow apps downloaded from: App Store and identified developers
    * Filevault: Enabled
    * Firewall: Enabled
* System Preferences > Energy Saver
    * Battery: 10 Minutes
    * Power Adapter: 20 Minutes
    * Powernap: Disabled
* System Preferences > Keyboard
    * Shortcuts > Mission Controller > Move left a space (Cmd+Left Arrow)
    * Shortcuts > Mission Controller > Move right a space (Cmd+Right Arrow)
* System Preferences > Mouse
    * Tracking Speed: 70%
    * Scrolling Speed: 40%
* System Preferences > Trackpad
    * Secondary click: Click with two fingers
    * Click: Medium
    * Tracking Speed: 60%
    * Scroll direction Natural: Disabled
* System Preferences 

